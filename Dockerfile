FROM node:10.14-alpine
RUN \
    yarn global add \
        babel-eslint \
        babel-loader \
        eslint \
        eslint-config-react-app \
        eslint-loader \
        eslint-plugin-flowtype \
        eslint-plugin-import \
        eslint-plugin-jsx-a11y \
        eslint-plugin-react \
    && \
    yarn cache clean

# Build arguments
ARG BUILD_DATE
ARG BUILD_REF

# Labels
LABEL \
    maintainer="Timmo <contact@timmo.xy\>" \
    org.label-schema.description="Eslint in a container for gitlab-ci" \
    org.label-schema.build-date=${BUILD_DATE} \
    org.label-schema.name="Eslint Pipeline" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.url="https://git.timmo.xyz" \
    org.label-schema.usage="https://gitlab.com/timmo001/eslint-pipeline/blob/master/README.md" \
    org.label-schema.vcs-ref=${BUILD_REF} \
    org.label-schema.vcs-url="https://gitlab.com/timmo001/eslint-pipeline/" \
    org.label-schema.vendor="Pipeline Components"
