# Eslint Pipeline

Docker image based on node:alpine for running eslint. Based on the
 [pipeline-components][pipeline-components] version but with added plugins
 for my specific React setup.

![Project Stage][project-stage-shield]
![Project Maintenance][maintenance-shield]
[![License][license-shield]](LICENSE)

[![GitLab CI][gitlabci-shield]][gitlabci]

## Docker status

[![Docker Version][version-shield]][microbadger]
[![Docker Layers][layers-shield]][microbadger]
[![Docker Pulls][pulls-shield]][dockerhub]

## Example usage

```yaml
eslint:
  stage: linting
  image: timmo001/eslint-pipeline:latest
  before_script:
    - touch dummy.js
  script:
    - eslint $( [[ -e .eslintrc ]] || echo '--no-eslintrc' ) --color .

```

Touching dummy.js prevents eslint from complaining that it had no files to lint

## Versioning

This project uses [Semantic Versioning][semver] for its version numbering.

## Support

Got questions?

You have several options to get them answered:
todo: addcommunication channel here

You could also [open an issue here][issue]

## Contributing

This is an active open-source project. We are always open to people who want to
use the code or contribute to it.

We've set up a separate document for our [contribution guidelines](CONTRIBUTING.md).

Thank you for being involved! :heart_eyes:

## Authors & contributors

The original setup of this repository is by [Timmo][timmo001].

For a full list of all authors and contributors,
check [the contributor's page][contributors].

## License

MIT License

Copyright (c) 2018 Timmo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[commits]: https://gitlab.com/timmo001/eslint-pipeline/commits/master
[contributors]: https://gitlab.com/timmo001/eslint-pipeline/graphs/master
[dockerhub]: https://hub.docker.com/r/timmo001/eslint-pipeline
[license-shield]: https://img.shields.io/badge/License-MIT-green.svg
[timmo001]: https://gitlab.com/timmo001
[gitlabci-shield]: https://img.shields.io/gitlab/pipeline/timmo001/eslint-pipeline.svg
[gitlabci]: https://gitlab.com/timmo001/eslint-pipeline/commits/master
[issue]: https://gitlab.com/timmo001/eslint-pipeline/issues
[keepchangelog]: http://keepachangelog.com/en/1.0.0/
[layers-shield]: https://images.microbadger.com/badges/image/timmo001/eslint-pipeline.svg
[maintenance-shield]: https://img.shields.io/maintenance/yes/2019.svg
[microbadger]: https://microbadger.com/images/timmo001/eslint-pipeline
[project-stage-shield]: https://img.shields.io/badge/project%20stage-production%20ready-brightgreen.svg
[pulls-shield]: https://img.shields.io/docker/pulls/timmo001/eslint-pipeline.svg
[releases]: https://gitlab.com/timmo001/eslint-pipeline/tags
[repository]: https://gitlab.com/timmo001/repository
[semver]: http://semver.org/spec/v2.0.0.html
[version-shield]: https://images.microbadger.com/badges/version/timmo001/eslint-pipeline.svg

[pipeline-components]: https://gitlab.com/pipeline-components
