---

.scripts: &scripts |
  # Loading docker build support scripts
  docker_build_with_builder() {
    docker build \
      --build-arg "BUILD_FROM=${FROM}" \
      --build-arg "BUILD_DATE=$(date +"%Y-%m-%dT%H:%M:%SZ")" \
      --build-arg "BUILD_ARCH=${BUILD_ARCH}" \
      --build-arg "BUILD_REF=${CI_COMMIT_SHA}" \
      --build-arg "BUILD_VERSION=${CI_COMMIT_TAG:-${CI_COMMIT_SHA:0:7}}" \
      --cache-from "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:build-cache" \
      --target build \
      --tag \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:build-${CI_COMMIT_SHA}" \
      --tag \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:build-cache" \
      .

      docker build \
      --build-arg "BUILD_FROM=${FROM}" \
      --build-arg "BUILD_DATE=$(date +"%Y-%m-%dT%H:%M:%SZ")" \
      --build-arg "BUILD_ARCH=${BUILD_ARCH}" \
      --build-arg "BUILD_REF=${CI_COMMIT_SHA}" \
      --build-arg "BUILD_VERSION=${CI_COMMIT_TAG:-${CI_COMMIT_SHA:0:7}}" \
      --cache-from "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:build-cache" \
      --cache-from "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:cache" \
      --tag \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:${CI_COMMIT_SHA}" \
      .

      docker push \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:build-${CI_COMMIT_SHA}"
      docker push \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:${CI_COMMIT_SHA}"
  }

  docker_build_oneshot() {

    docker build \
      --build-arg "BUILD_FROM=${FROM}" \
      --build-arg "BUILD_DATE=$(date +"%Y-%m-%dT%H:%M:%SZ")" \
      --build-arg "BUILD_ARCH=${BUILD_ARCH}" \
      --build-arg "BUILD_REF=${CI_COMMIT_SHA}" \
      --build-arg "BUILD_VERSION=${CI_COMMIT_TAG:-${CI_COMMIT_SHA:0:7}}" \
      --cache-from "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:cache" \
      --tag \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:${CI_COMMIT_SHA}" \
      .

      docker push \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:${CI_COMMIT_SHA}"

  }

  docker_build(){
    if [ "$(grep -c -E "^FROM.+as build" ./Dockerfile)" -eq 1 ] ; then
      docker_build_with_builder
    else
      docker_build_oneshot
    fi
  }

  docker_pull_cache(){
    if [ "$(grep -c -E "^FROM.+as build" ./Dockerfile)" -eq 1 ] ; then
      docker pull "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:build-cache" || true
    fi
    docker pull "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:cache" || true
  }

  docker_push_cache(){
    if [ "$(grep -c -E "^FROM.+as build" ./Dockerfile)" -eq 1 ] ; then
      docker tag \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:build-${CI_COMMIT_SHA}" \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:build-cache"
      docker push "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:build-cache"
    fi
    docker tag \
      "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:${CI_COMMIT_SHA}" \
      "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:cache"
    docker push "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:cache"
  }

  docker_pull_build(){
    if [ "$(grep -c -E "^FROM.+as build" ./Dockerfile)" -eq 1 ] ; then
      docker pull "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:build-${CI_COMMIT_SHA}"
    fi
    docker pull "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:${CI_COMMIT_SHA}"
  }

  docker_prepare () {
    if ! docker info &>/dev/null; then
      if [ "$KUBERNETES_PORT" ]; then
        export DOCKER_HOST='tcp://localhost:2375'
      fi
    fi
    docker info
  }

  docker_ci_login () {
    echo "${CI_JOB_TOKEN}" | docker login \
      --username gitlab-ci-token \
      --password-stdin \
      "${CI_REGISTRY}"
  }

  docker_hub_login() {
    echo "${DOCKER_PASSWORD}" | docker login \
      --username "${DOCKER_LOGIN}" \
      --password-stdin
  }


services:
  - docker:dind

variables:
  IMAGE_NAME: ${CI_REGISTRY}/$CI_PROJECT_PATH
  IMAGE_TAG: ${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHA}
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_HUB_ORG: timmo001

stages:
  - preflight
  - build
  - scan
  - deploy
  - manifest
  - monitor

# Generic DIND template
.dind: &dind
  image: docker:latest
  tags:
    - docker
  before_script:
    - *scripts
    - docker_prepare

  services:
    - name: docker:dind
      command: ["--experimental"]

# Generic preflight template
.preflight: &preflight
  stage: preflight

# Generic build template
.build: &build
  <<: *dind
  stage: build
  tags:
    - docker
  before_script:
    - *scripts
    - docker_prepare
    - docker_ci_login
    - docker_pull_cache
  script:
    - docker_build

# Generic scan template
.scan: &scan
  <<: *dind
  stage: scan
  tags:
    - docker

.scanclair: &scanclair
  <<: *scan
  stage: scan
  allow_failure: true
  tags:
    - docker
  before_script:
    - *scripts
    - |
      export CLAIRHOST='docker'
      if ! docker info &>/dev/null; then
        if [ "$KUBERNETES_PORT" ]; then
          export DOCKER_HOST='tcp://localhost:2375'
          export CLAIRHOST='localhost'
        fi
      fi
    - docker info
    - docker_ci_login
    - docker run -d --name db --restart on-failure arminc/clair-db:latest
    - |
      docker run -p 6060:6060 --link db:postgres -d --name clair \
        --restart on-failure arminc/clair-local-scan:v2.0.1
    - apk add -U curl ca-certificates wget
    - |
      curl \
        --silent \
        --show-error \
        --location \
        --fail \
        --retry 3 \
        --output /usr/bin/clair-scanner \
        https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
    - chmod +x /usr/bin/clair-scanner
    - touch clair-whitelist.yml
    - docker pull "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:${CI_COMMIT_SHA}"
    - while( ! wget -q -O /dev/null http://${CLAIRHOST}:6060/v1/namespaces ) ; do sleep 1 ; done
    - retries=0
    - echo "Waiting for clair daemon to start"
    - |
      while ( ! wget -T 10 -q -O /dev/null http://${CLAIRHOST}:6060/v1/namespaces ) ; do \
        sleep 1 ; \
        echo -n "." ; \
        if [ $retries -eq 10 ] ; then  \
          echo " Timeout, aborting." ; \
          docker logs db
          docker logs clair
          exit 1 ; \
        fi ; \
        retries=$(($retries+1)) ; \
      done
  script:
    - |
      clair-scanner \
        -c http://${CLAIRHOST}:6060 \
        --ip $(hostname -i) \
        -w clair-whitelist.yml \
        -r gl-container-scanning-report.json \
        -l clair.log \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:${CI_COMMIT_SHA}"
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json

.scansnykdocker: &scansnykdocker
  <<: *scan
  image: pipelinecomponents/snyk:edge
  allow_failure: true
  before_script:
    - apk add --no-cache docker
    - *scripts
    - docker_prepare
    - docker_ci_login
    - docker_pull_build
    - docker info
  script:
    - snyk test --docker "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:${CI_COMMIT_SHA}" --file=Dockerfile

.scansnyk: &scansnyk
  stage: scan
  image: pipelinecomponents/snyk:edge
  allow_failure: true
  script:
    - >-
      find .
      -name ${SCANFILE}  -print0 |
      xargs -0 -r -t -I {}
      snyk test
      --org=timmo001
      --project-name=$CI_PROJECT_PATH
      --file={}

# Generic deploy template
.deploy: &deploy
  <<: *dind
  stage: deploy
  tags:
    - docker
  before_script:
    - *scripts
    - docker_prepare
    - docker_ci_login
    - docker_hub_login
    - docker_pull_build
  script:
    - docker_push_cache
    - TAG="${CI_COMMIT_TAG#v}"
    - TAG="${TAG:-${CI_COMMIT_SHA:0:7}}"
    - |
      docker tag \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:${CI_COMMIT_SHA}" \
        "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:${BUILD_ARCH}-${TAG}"
    - |
      docker push \
        "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:${BUILD_ARCH}-${TAG}"
    - |
      docker tag \
        "${CI_REGISTRY_IMAGE}/${BUILD_ARCH}:${CI_COMMIT_SHA}" \
        "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}-${BUILD_ARCH}:${TAG}"
    - |
      docker push \
        "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}-${BUILD_ARCH}:${TAG}"
  only:
    - master
    - /^v\d+\.\d+\.\d+(?:-(?:beta|rc)(?:(?:(?:\+|\.)?[a-zA-Z0-9]+)*)?)?$/
  except:
    - /^(?!master).+@/

# Generic manifest template
.manifest: &manifest
  <<: *dind
  stage: manifest
  tags:
    - docker
  before_script:
    - *scripts
    - mkdir -p ~/.docker
    - echo '{"experimental":"enabled"}' > ~/.docker/config.json
    - docker_prepare
    - docker_hub_login
  script:
    - TAG="${TAG#v}"
    - TAG="${TAG:-${CI_COMMIT_SHA:0:7}}"
    - REF="${CI_COMMIT_TAG#v}"
    - REF="${REF:-${CI_COMMIT_SHA:0:7}}"
    - |
      docker manifest create \
        "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:${TAG}" \
        "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:amd64-${REF}"
    - |
      docker manifest annotate \
        "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:${TAG}" \
        "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:amd64-${REF}" \
        --os=linux \
        --arch=amd64
    - |
      docker manifest push \
        "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:${TAG}"
  except:
    - /^(?!master).+@/

.monitorsnyk: &monitorsnyk
  <<: *dind
  stage: monitor
  image: pipelinecomponents/snyk:edge
  variables:
    TAG: latest
    BUILD_ARCH: amd64
  only:
    - /^v\d+\.\d+\.\d+(?:(?:(?:\+|\.)?[a-zA-Z0-9]+)*)?$/
  before_script:
    - *scripts
    - docker_prepare
    - docker info
    - docker pull ${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:${TAG}
  script:
    - >-
      snyk monitor --org="timmo001"
      --project-name="${CI_PROJECT_PATH}"
      --docker "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:${TAG}"
      --file=Dockerfile

# Preflight jobs
hadolint:
  <<: *preflight
  image: pipelinecomponents/hadolint:latest
  before_script:
    - hadolint --version
  script:
    - hadolint "Dockerfile"

shellcheck:
  <<: *preflight
  image: pipelinecomponents/shellcheck:latest
  script:
    # anything ending on .sh, should be shell script
    - |
      find . -name .git -type d -prune -o -type f  -name \*.sh -print0 |
      xargs -0 -P $(nproc) -r -n1 shellcheck
    # magic, any file with a valid shebang, is alloed to be scanned aswell
    - |
      find . -name .git -type d -prune -o -type f  -regex '.*/[^.]*$'   -print0 |
      xargs -0 -P $(nproc) -r -n1 sh -c 'FILE="$0"; if head -n1 "$FILE" |grep -q "^#\\! \?/.\+\(ba|d|k\)\?sh" ; then shellcheck "$FILE" ; else /bin/true ; fi '

yamllint:
  <<: *preflight
  image: pipelinecomponents/yamllint:latest
  before_script:
    - yamllint --version
  script:
    - yamllint .

jsonlint:
  <<: *preflight
  image: pipelinecomponents/jsonlint:latest
  before_script:
    - jsonlint --version || true
  script:
    - |
      find . -not -path './.git/*' -name '*.json' -type f -print0 |
      parallel --will-cite -k -0 -n1 jsonlint -q

markdownlint:
  <<: *preflight
  image: pipelinecomponents/markdownlint:latest
  before_script:
    - mdl --version
  script:
    - mdl --style all --warnings .

# Build Jobs
build amd64:
  <<: *build
  variables:
    BUILD_ARCH: amd64

# Scan jobs
clair amd64:
  <<: *scanclair
  variables:
    BUILD_ARCH: amd64

snyk docker amd64:
  <<: *scansnykdocker
  variables:
    BUILD_ARCH: amd64

snyk yarn:
  <<: *scansnyk
  variables:
    SCANFILE: yarn.lock

snyk npm:
  <<: *scansnyk
  variables:
    SCANFILE: package-lock.json

snyk ruby:
  <<: *scansnyk
  variables:
    SCANFILE: Gemfile.lock

snyk pip:
  <<: *scansnyk
  variables:
    SCANFILE: requirements.txt

snyk composer:
  <<: *scansnyk
  variables:
    SCANFILE: composer.lock

# Deploy jobs
deploy amd64:
  <<: *deploy
  variables:
    BUILD_ARCH: amd64

# Manifest jobs
manifest sha:
  <<: *manifest
  only:
    - master

manifest version:
  <<: *manifest
  variables:
    TAG: "${CI_COMMIT_TAG}"
  only:
    - /^v\d+\.\d+\.\d+(?:-(?:beta|rc)(?:(?:(?:\+|\.)?[a-zA-Z0-9]+)*)?)?$/

manifest stable:
  <<: *manifest
  variables:
    TAG: latest
  only:
    - /^v\d+\.\d+\.\d+(?:(?:(?:\+|\.)?[a-zA-Z0-9]+)*)?$/
  after_script:
    - docker pull ${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:latest
    - |
      DESC="$(docker inspect ${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:latest \
            -f '{{ index .Config.Labels "org.label-schema.description" }}')";
    - |
      docker run -w /code/ -v $(pwd):/code/ --rm \
        -e DOCKER_USER="${DOCKER_LOGIN}" -e DOCKER_PASS="${DOCKER_PASSWORD}" \
        mjrider/docker-update /app/dockerupdate.py \
        "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}" "${DESC}" README.md

manifest beta:
  <<: *manifest
  variables:
    TAG: beta
  only:
    - /^v\d+\.\d+\.\d+(?:-(?:beta|rc)(?:(?:(?:\+|\.)?[a-zA-Z0-9]+)*)?)?$/

manifest edge:
  <<: *manifest
  variables:
    TAG: edge
  only:
    - master

monitor snyk docker:
  <<: *monitorsnyk
  only:
    - /^v\d+\.\d+\.\d+(?:(?:(?:\+|\.)?[a-zA-Z0-9]+)*)?$/
